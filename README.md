# horti

A telemetry collector for the EasyGrow 3000 horticultural controller, sending collected data 
via [Apache Camel](http://camel.apache.org/) to [Graphite](https://graphiteapp.org/) for display 
using any of its popular frontends.

![Easygrow controller](/images/easygrow.jpg)

## Features

* Collects per-minute telemetry data from controller showing current readings for temperature, 
  humidity etc. and also current settings.
* Uses [Apache Camel](http://camel.apache.org/) for integration
* Features either direct access to [Graphite server](https://graphiteapp.org/) ('developer mode') or 
  reliable connections over unreliable networks using durable [Active MQ](http://activemq.apache.org/)
  queues (this will be the default mode for use by customers).
* Telemetry either collected from CSV files ('developer mode') or directly from serial port.
* In order to track changes in the data format during development, the data input is completely defined
  in a YAML configuration file, [see example](https://gitlab.com/marsbard/camel-horti-graphite/blob/master/src/main/resources/data1Fields.yml)

### Running

The main class for the client is `com.bettercode.horti.client.Client`.

Please see also [horti-server-testing](https://gitlab.com/marsbard/horti-server-testing) for a trivial
implementation of a server which can receive the messages as a server (but the messages are thrown away
once they hit the server).

### Running in eclipse

There are 3 projects, this one `horti-client`, the trivial server implementation 
`horti-server-testing` and common (or to-be-common) dependencies in `horti-common`.

In an eclipse workspace, for each of the following 3 repositories, do "New Project" (Ctrl-N) and
from the wizard choose "Maven" | "Check out Maven projects from SCM". Choose 'git' from the 
dropdown at `SCM URL` and paste the git URL from below. (If there is no 'git' selection in the
dropdown click the link that says "Find more SCM connectors in the m2e marketplace" and choose
"Maven SCM Handler for EGit")

* git@gitlab.com:marsbard/horti-client.git
* git@gitlab.com:marsbard/horti-common.git
* git@gitlab.com:marsbard/horti-server-testing.git

The above links require you to have an account on gitlab with commit permissions. If you just want to 
check out the projects without being able to commit your changes you can use the following URIs:

* https://gitlab.com/marsbard/horti-client.git
* https://gitlab.com/marsbard/horti-common.git
* https://gitlab.com/marsbard/horti-server-testing.git

Set up two Run configurations: Run | Run Configurations ... - choose type "Java Application", and click 
the 'New' button once for each configuration:

* Name: Client, Project: horti-client, Main class: com.bettercode.horti.client.Client
* Name: Server, Project: horti-server, Main class: com.bettercode.horti.server.Server

