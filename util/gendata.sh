#!/bin/bash

. `dirname $0`/funcs

if [ -z $1 ]
then
    echo First argument must be path to put data files
    exit 1
fi

OUTDIR=$1

if [ $OUTDIR != "-" -a ! -d $OUTDIR ]
then
    echo "'$OUTDIR' is not a directory"
    exit 2
fi


# data.input.headers=in_Light,in_co2,in_humid,in_temp,out_co2,out_lights,out_ch2,out_heater,out_cooling,out_humid,out_dehumid,time
ILUX=76
ICO2=550
IHUM=428
ITMP=285
OCO2=0
OLUX=1
OCH2=1
OHEA=0
OCOO=1
OHUM=0
ODEH=1

HOUR=12

for HOUR in `seq 0 23`
do
for MIN in `seq 0  59`
do
    TIME=`printf "%02d%02d" $HOUR $MIN`
    OLUX=`ranswitch $OLUX 5`
    
    if [ $OLUX == 0 ]
    then
        ILUX=0
    else
        if [ $ILUX == 0 ]
    then 
        ILUX=85
            fi
    ILUX=`ranmod $ILUX 8 99 `
    fi

    #OCO2=`ranswitch $OCO2 5`

    if [ $OCO2 == 1 ]
    then
     ICO2=$(( ICO2 + 60 )) 
            ICO2=`ranmod $ICO2 2 950`
    else
     ICO2=$(( ICO2 - 10 )) 
            ICO2=`ranmod $ICO2 2 950`
    fi

    if [ $ICO2 -lt 400 ]
    then
    OCOO=0
            OCO2=1
    fi

    if [ $ICO2 -gt 600 ]
    then
            OCOO=1
            OCO2=0
    fi

    if [ $OLUX == 1 ]
    then
        IHUM=$(( $IHUM + 30 ))
        IHUM=`ranmod $IHUM 10 990`
    fi        

    if [ $OHUM == 1 ]
    then
        IHUM=$(( $IHUM + 20 ))
    else
        IHUM=$(( $IHUM + 20 ))
    fi
    IHUM=`ranmod $(( $IHUM )) 30 990`

    if [ $ODEH == 1 ]
    then
        IHUM=`ranmod $(( $IHUM - 30 )) 10 990`
    else
        IHUM=`ranmod $IHUM 10 990`
    fi
        
    if [ $IHUM -gt 550 ]
    then
        ODEH=1
    else
        ODEH=0
    fi

    if [ $IHUM -lt 350 ]
    then
        OHUM=1
    else
        OHUM=0
    fi

    if [ $OLUX == 1 ]
    then
     ITMP=$(( ITMP + 2 ))
    else
     ITMP=$(( ITMP - 1 ))
    fi

    if [ $OHEA == 1 ]
    then
          ITMP=$(( $ITMP + 2 ))
    fi

    if [ $OCOO == 1 ]
    then
          ITMP=$(( $ITMP - 2 ))
    fi


    if [ $ITMP -gt 350 ]
    then
        OLUX=0
        OCOO=1
    fi

    if [ $ITMP -lt 300 ]
    then 
        OCOO=0
    fi

    if [ $ITMP -lt 200 ]
    then
        OHEA=1
    else
        OHEA=0
    fi
 
    OUT="R,$ILUX,$ICO2,$IHUM,$ITMP,$OCO2,$OLUX,$OCH2,$OHEA,$OCOO,$OHUM,$ODEH,0,${TIME}0" 
    if [ $OUTDIR != "-" ]
    then
        echo $OUT > $OUTDIR/$TIME.csv
    fi
    echo $OUT
done
done
