package com.bettercode.horti.dataFields;

import java.io.IOException;
import java.util.List;

import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.entities.DataField;

public interface FieldLoader {

	public List<DataField> getFields() throws HortiException, IOException;
}
