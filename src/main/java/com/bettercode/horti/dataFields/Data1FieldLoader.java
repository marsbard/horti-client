package com.bettercode.horti.dataFields;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.entities.DataField;

public class Data1FieldLoader implements FieldLoader
{
	private static final Logger log = Logger.getLogger(Data1FieldLoader.class);

	private String configClasspathLocation = "";

	private List<DataField> fields;
	
	public void loadFields() throws HortiException, IOException {
		log.info("Loading field definitions from " + configClasspathLocation);

		Yaml yaml = new Yaml();
		
		fields = new ArrayList<DataField>();
		
		try(InputStream stream = getClass().getResourceAsStream(configClasspathLocation)) {
		
			@SuppressWarnings("unchecked")
			List<Map<String,String>> data = (List<Map<String, String>>) yaml.load(stream);
			log.debug(data);
			
			for(Map<String,String> field: data){
				fields.add(new DataField(field));
			}
		} 		
	}

	public List<DataField> getFields() throws HortiException, IOException{
		if(fields==null){
			loadFields();
		}
		return fields;
	}
	
	public void setConfigClasspathLocation(String filePath) {
		this.configClasspathLocation = filePath;
	}

}
