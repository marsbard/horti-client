package com.bettercode.horti.client;

import java.util.Properties;

import com.bettercode.horti.common.HortiException;

public class Config {
	static final String KEY_GRAPHITE_MODE = "graphite.mode";
	private static final String KEY_GRAPHITE_HOST = "graphite.server.host";
	private static final String KEY_GRAPHITE_PORT = "graphite.server.port";

	public static final int MODE_DIRECT = 1;
	public static final int MODE_QUEUED = 2;
	private static final String KEY_QUEUE_HOST = "queue.server.host";
	private static final String KEY_QUEUE_PORT = "queue.server.port";
	private static final String KEY_CLIENT_ID = "easygrow.serialnum";
	
	private Properties properties;
	private int mode;

	public Config(Properties properties) throws HortiException {
		this.properties = properties;

		String inMode = properties.getProperty(KEY_GRAPHITE_MODE);
		switch (inMode) {
		case "direct":
			checkRelatedPropertiesDirectMode();
			setMode(Config.MODE_DIRECT);
			break;
		case "queued":
			checkRelatedPropertiesQueuedMode();
			setMode(Config.MODE_QUEUED);
			break;
		default:
			throw new HortiException(String.format("Unknown mode '%s'", inMode));
		}
	}

	private void checkRelatedPropertiesQueuedMode() throws HortiException {
		String host = properties.getProperty(KEY_QUEUE_HOST);
		String port = properties.getProperty(KEY_QUEUE_PORT);

		if (host == null || host.equals("")) {
			throw new HortiException(String.format("Property '%s' must be set when '%s' is 'queued'", KEY_QUEUE_HOST,
					KEY_GRAPHITE_MODE));
		}
		if (port == null || port.equals("")) {
			throw new HortiException(String.format("Property '%s' must be set when '%s' is 'queued'", KEY_QUEUE_PORT,
					KEY_GRAPHITE_MODE));
		}
	}

	private void checkRelatedPropertiesDirectMode() throws HortiException {
		String host = properties.getProperty(KEY_GRAPHITE_HOST);
		String port = properties.getProperty(KEY_GRAPHITE_PORT);

		if (host == null || host.equals("")) {
			throw new HortiException(String.format("Property '%s' must be set when '%s' is 'direct'", KEY_GRAPHITE_HOST,
					KEY_GRAPHITE_MODE));
		}
		if (port == null || port.equals("")) {
			throw new HortiException(String.format("Property '%s' must be set when '%s' is 'direct'", KEY_GRAPHITE_PORT,
					KEY_GRAPHITE_MODE));
		}
	}

	private void setMode(int mode) {
		this.mode = mode;
	}

	public int getMode() {
		return mode;
	}

	public String getGraphiteHost() {
		return properties.getProperty(KEY_GRAPHITE_HOST);
	}

	public int getGraphitePort() {
		return Integer.parseInt(properties.getProperty(KEY_GRAPHITE_PORT));
	}

	public String getQueueHost() {
		return properties.getProperty(KEY_QUEUE_HOST);
	}

	public int getQueuePort() {
		return Integer.parseInt(properties.getProperty(KEY_QUEUE_PORT));

	}

	public String getClientId() {
		return properties.getProperty(KEY_CLIENT_ID);
	}
}
