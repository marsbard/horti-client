package com.bettercode.horti.client.routes;

import java.io.IOException;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.camel.Exchange;
import org.apache.camel.Expression;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.ServiceStatus;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import com.bettercode.horti.client.Client;
import com.bettercode.horti.client.Config;
import com.bettercode.horti.common.HortiException;

public class ClientRoutes extends RouteBuilder {

	private static final String ROUTE_RETRY = "route-retry";
	final private static Logger log = Logger.getLogger(ClientRoutes.class);
	private static final Long RETRY_DELAY = 50L;
	protected static final String HEADER_CACHED_NAME = "X-Already-Cached";

	@Override
	public void configure() throws Exception {

		Properties props = Client.getInstance().getProperties();

		String fileUri = String.format("file://%s?delete=true", props.getProperty("data.input.location"));

		Config config = new Config(props);

		switch (config.getMode()) {
		case Config.MODE_DIRECT:
			String graphiteUri = String.format("graphite:%s:%s?prefix=%s",
					props.getProperty("graphite.server.host", "localhost"),
					props.getProperty("graphite.server.port", "2003"),
					props.getProperty("graphite.prefix", "horti.telemetry"));
			log.info(String.format("graphiteUri %s", graphiteUri));
			from(fileUri).unmarshal().csv().to(graphiteUri);
			break;
		case Config.MODE_QUEUED:

			String queueUri = String.format("amqremote:queue:telemetry?clientId=%s", config.getClientId());
			String cacheUri = "amqlocal:queue:TELEMETRY.CACHE?";

			onException(JMSException.class).handled(true).routeId("route-jmsexception")
					/*
					 * Only add the message to the retry queue if it has not
					 * already been cached
					 */
					.filter(header(HEADER_CACHED_NAME).isNull()).process(new Processor() {
						@Override
						public void process(Exchange exchange) throws Exception {

							Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
							log.error(String.format(
									"Caught a '%s' while sending, caching until remote is back, message was: %s",
									cause.getClass().getName(), cause.getMessage()));
							exchange.getIn().setHeader(HEADER_CACHED_NAME, true);

						}

					})
					/*
					 * disable the retry route until ActiveMQ connection is
					 * online
					 * https://camel.apache.org/how-can-i-stop-a-route-from-a-
					 * route.html
					 */
					.process(new Processor() {

						@Override
						public void process(final Exchange exchange) throws Exception {
							Client.getRouteManager().disableRoute(ROUTE_RETRY);
						}

					}).to(cacheUri);

			
			from(cacheUri).routeId(ROUTE_RETRY).delay(RETRY_DELAY).process(new Processor() {

				@Override
				public void process(Exchange exchange) throws Exception {
					log.debug(String.format("Retrying send of %s", exchange.getIn().getBody()));
				}

			}).to(queueUri);

			from(fileUri).routeId("route-fileinput").unmarshal().csv().to(queueUri)


					/*
					 * restart the retry route, presume if we got here the
					 * connection to activemq is up
					 */
					.process(new Processor() {

						@Override
						public void process(final Exchange exchange) throws Exception {

							Client.getRouteManager().enableRoute(ROUTE_RETRY);

						}
					});

			break;
		default:
			String msg = "Bad mode: " + config.getMode();
			log.error(msg);
			throw new HortiException(msg);
		}
	}

}
