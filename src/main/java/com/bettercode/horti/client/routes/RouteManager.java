package com.bettercode.horti.client.routes;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.ServiceStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.bettercode.horti.common.HortiException;

public class RouteManager {

	public enum RouteState {
		Disabled, Enabled
	}

	private static final Logger log = Logger.getLogger(RouteManager.class);

	private List<String> disabledRoutes;

	private CamelContext context;

	public RouteManager() {
		disabledRoutes = new ArrayList<String>();
	}

	@Autowired
	public void setContext(CamelContext context) {
		log.info("Setting CamelContext");
		this.context = context;
	}

	public boolean isDisabled(String routeName) {
		if (disabledRoutes.contains(routeName)) {
			return true;
		}
		return false;
	}

	public void disableRoute(String routeName) throws HortiException {
		if (!disabledRoutes.contains(routeName)) {
			disabledRoutes.add(routeName);

			controlRoute(routeName, RouteState.Disabled);
		}
	}

	public void enableRoute(String routeName) throws HortiException {
		disabledRoutes.remove(routeName);
		controlRoute(routeName, RouteState.Enabled);
	}

	private void controlRoute(String routeName, RouteState state) throws HortiException {
		switch (state) {
		case Disabled:
			stopRoute(routeName);
			break;
		case Enabled:
			startRoute(routeName);
			break;
		}
	}

	private void startRoute(String routeName) throws HortiException {
		ServiceStatus status = context.getRouteStatus(routeName);
		if (status.equals(ServiceStatus.Started)) {
			return;
		}
		log.info("Resuming route " + routeName);
		try {
			context.resumeRoute(routeName);
		} catch (Exception e) {
			throw new HortiException(e);
		}
	}

	private void stopRoute(final String routeName) {
		Thread stop = null;
		ServiceStatus status = context.getRouteStatus(routeName);

		if (status.equals(ServiceStatus.Suspended) || status.equals(ServiceStatus.Suspending)) {
			return;
		}

		log.info("Suspending route " + routeName);
		// stop this route using a thread that will stop
		// this route gracefully while we are still running
		if (stop == null) {
			stop = new Thread() {
				@Override
				public void run() {
					try {
						context.suspendRoute(routeName);
					} catch (Exception e) {
						// ignore
					}
				}
			};
		}

		// start the thread that stops this route
		stop.start();

		try {
			// settle down
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
	}


}
