package com.bettercode.horti.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bettercode.horti.client.routes.RouteManager;
import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.dataFields.FieldLoader;
import com.bettercode.horti.entities.DataField;
import com.bettercode.horti.graphite.NetClient;

public class Client {

	private static Config config;

	private static ApplicationContext context;


	private static FieldLoader fieldLoader;

	private static List<DataField> fields;

	public static NetClient graphiteClient;

	private static Client instance;

	private static RouteManager routeManager;
	
	@SuppressWarnings("unused")
	final private static Logger log = Logger.getLogger(Client.class);

	public static void setRouteManager(RouteManager routeManager) {
		Client.routeManager = routeManager;
	}
	public static RouteManager getRouteManager() {
		return routeManager;
	}


	
	public static Config getConfig() {
		return config;
	}

	public static FieldLoader getFieldLoader() {
		return fieldLoader;
	}

	public static List<DataField> getFields() {
		return fields;
	}

	public static Client getInstance() throws Exception {
		if (instance == null) {
			instance = new Client();
			instance.setup();
		}
		return instance;
	}

	public static void main(String[] args) throws Exception {
		context = new ClassPathXmlApplicationContext("/META-INF/spring/context.xml");
		log.info(context);
		Thread.sleep(1000);
	}

	private static void setConfig(Config config) {
		Client.config = config;
	}

	public static void setFieldLoader(FieldLoader fieldLoader) {
		Client.fieldLoader = fieldLoader;
	}

	public static void setGraphiteClient(NetClient graphiteClientIn) {
		graphiteClient = graphiteClientIn;
	}

	private String[] headers;

	final Properties properties = new Properties();

	public String[] getHeaders() {
		return headers;
	}

	public Properties getProperties() {
		return properties;
	}

	private void setup() throws Exception {
		try (final InputStream stream = this.getClass().getResourceAsStream("/config.properties")) {
			properties.load(stream);
		}

		fields = fieldLoader.getFields();
		setConfig(new Config(properties));

		switch(config.getMode()){
		case Config.MODE_DIRECT:
			setupGraphiteClient();
			break;
		case Config.MODE_QUEUED:
			setupQueueClient();
			break;
		default:
			throw new HortiException("Unknown mode passed: " + config.getMode());
		}

	}

	private void setupQueueClient() {
	}

	private void setupGraphiteClient() throws IOException, HortiException {
		String graphiteHost = config.getGraphiteHost();
		int graphitePort = config.getGraphitePort();
		log.info(String.format("Mode is 'direct', attempting connection to graphite host at %s:%d",
				graphiteHost, graphitePort));
		
		/*
		 * we try to connect here but don't send anything, just to check that there is something
		 * listening where we expect it, then we close it; in practice we open and close the 
		 * graphiteClient each time we send the telemetry for a whole row (once per minute)
		 */
		try {
			graphiteClient.connect(graphiteHost, graphitePort);
		} catch (IOException e) {
			String msg = String.format("Graphite mode was set to 'direct' but no graphite server found on %s:%d",
					graphiteHost, graphitePort);
			log.error(msg);
			throw new HortiException(msg, e);
		} finally {
			graphiteClient.close();
		}		
	}
}
