/**
 * 
 */
package com.bettercode.horti;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.common.TimeUtil;

/**
 * @author marsbard
 *
 */
public class TimeUtilTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}
	@Rule
	// https://github.com/junit-team/junit4/wiki/exception-testing#expectedexception-rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testGetTimestamp() {
		/*
		    public DateTime(
            int year,
            int monthOfYear,
            int dayOfMonth,
            int hourOfDay,
            int minuteOfHour,
            DateTimeZone timezone)
		 */
		DateTime testTime = new DateTime(2012,1,31,12,34, DateTimeZone.UTC);
		String testTimeStr = "2012-01-31 12:34";
		
		DateTime chk = TimeUtil.getTimestamp("YYYY-MM-DD hh:mm", testTimeStr);
		assert(testTime.equals(chk));
		
	}
	
	
	
	@Test
	public void test_hhmm0() throws HortiException {
		String format = "hhmm0"; // first format we need to deal with
		
		assertTrue("12340 is OK", TimeUtil.checkTimeFormat(format, "12340")); 

		String[] passes = {"12340"};
		assertBools(passes, format, true);
		
		String[] fails = { "123400", "12345" }; 
		assertBools(fails, format, false);
	}
	

	@Test
	public void test_YYMMDD() throws HortiException{
		String format = "YYYY-MM-DD";
		
		assertTrue("1980-12-31 is OK", TimeUtil.checkTimeFormat(format, "1980-12-31"));
	
		String[] passes = { "1980-12-31", "2012-06-06" };
		assertBools(passes, format, true);
		
		
		String[] fails = { "19801-12-31", "1980-112-31", "1980-12-311", "1980/12/31", "12-12-1980" };
		assertBools(fails, format, false);
		
	}
	
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	private void assertBools(String[] tries, String format, boolean trueness) throws HortiException {
		if(trueness){
			for (String value: tries){
				assertTrue(value + " passes", TimeUtil.checkTimeFormat(format, value));
			}
		} else {
			for (String value: tries){
				assertFalse(value + " fails", TimeUtil.checkTimeFormat(format, value));
			}			
		}
	}
}
