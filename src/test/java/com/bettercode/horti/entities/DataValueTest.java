package com.bettercode.horti.entities;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by martian on 8/01/17.
 */
public class DataValueTest {

    @Test
    public void testDataValue(){
        DataValue dv = new DataValue();
        DataField df = new DataField();
        df.setName("testField");
        
        dv.setField(df);
        dv.setValue("testValue");
        
        assertEquals(dv.getField(), df);
        assertEquals(dv.getValue(), "testValue");
    }

}