package com.bettercode.horti;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.bettercode.horti.client.Config;
import com.bettercode.horti.common.HortiException;

public class ConfigTest {

	private static final String KEY_MODE = "graphite.mode";

	private static final String STR_MODE_DIRECT = "direct";
	private static final String STR_MODE_QUEUED = "queued";

	private static final String KEY_GRAPHITE_HOST = "graphite.server.host";
	private static final String KEY_GRAPHITE_PORT = "graphite.server.port";

	private static final String STR_GRAPHITE_HOST = "localhost";
	private static final String STR_GRAPHITE_PORT = "2003";

	private static final String KEY_QUEUE_HOST = "queue.server.host";
	private static final String KEY_QUEUE_PORT = "queue.server.port";
	private static final String STR_QUEUE_HOST = "localhost";
	private static final String STR_QUEUE_PORT = "61616";

	private static final String KEY_CLIENT_ID = "easygrow.serialnum";


	
	private Properties properties;
	

	@Before
	public void setup(){
		properties = new Properties();
	}
	
	@Test
	public void testConfigModeDirect() throws HortiException {
		properties.setProperty(KEY_MODE, STR_MODE_DIRECT);
		properties.setProperty(KEY_GRAPHITE_HOST, STR_GRAPHITE_HOST);
		properties.setProperty(KEY_GRAPHITE_PORT, STR_GRAPHITE_PORT);
		Config config = new Config(properties);
		
		assertEquals(Config.MODE_DIRECT, config.getMode());
	}

	@Test
	public void testConfigModeQueued() throws HortiException {
		properties.setProperty(KEY_MODE, STR_MODE_QUEUED);
		properties.setProperty(KEY_QUEUE_HOST, STR_QUEUE_HOST);
		properties.setProperty(KEY_QUEUE_PORT, STR_QUEUE_PORT);
		Config config = new Config(properties);
		
		assertEquals(Config.MODE_QUEUED, config.getMode());
	}

	@Test(expected = HortiException.class)
	public void testConfigModeBad() throws HortiException {
		properties.setProperty(KEY_MODE, "rubbish");
		
		@SuppressWarnings("unused")
		Config config = new Config(properties);

	}
	
	@Test(expected = HortiException.class)
	public void testConfigModeDirectRelatedPropertiesNotPresent() throws HortiException{
		properties.setProperty(KEY_MODE, STR_MODE_DIRECT);

		properties.setProperty(KEY_GRAPHITE_HOST, "");
		properties.setProperty(KEY_GRAPHITE_PORT, "");
		@SuppressWarnings("unused")
		Config config = new Config(properties);
			
	}	
	
	
	@Test
	public void testConfigModeDirectRelatedPropertiesPresent() throws HortiException{
		properties.setProperty(KEY_MODE, STR_MODE_DIRECT);
		properties.setProperty(KEY_GRAPHITE_HOST, STR_GRAPHITE_HOST);
		properties.setProperty(KEY_GRAPHITE_PORT, STR_GRAPHITE_PORT);
		
		Config config = new Config(properties);
		assertEquals(Config.MODE_DIRECT, config.getMode());
		
		assertEquals(STR_GRAPHITE_HOST, config.getGraphiteHost());
		assertEquals(Integer.parseInt(STR_GRAPHITE_PORT), config.getGraphitePort());

		assertNotEquals(0, config.getGraphitePort());

	}

	
	@Test
	public void testConfigModeQueuedRelatedPropertiesPresent() throws HortiException{
		properties.setProperty(KEY_MODE, STR_MODE_QUEUED);
		properties.setProperty(KEY_QUEUE_HOST, STR_QUEUE_HOST);
		properties.setProperty(KEY_QUEUE_PORT, STR_QUEUE_PORT);
		
		Config config = new Config(properties);
		assertEquals(Config.MODE_QUEUED, config.getMode());
		
		assertEquals(STR_QUEUE_HOST, config.getQueueHost());
		assertEquals(Integer.parseInt(STR_QUEUE_PORT), config.getQueuePort());
		assertNotEquals(0, config.getQueuePort());
		
	}
	
	@Test
	public void testClientId() throws HortiException{
		properties.setProperty(KEY_CLIENT_ID, "abc123");
		properties.setProperty(KEY_MODE, STR_MODE_QUEUED);
		properties.setProperty(KEY_QUEUE_HOST, STR_QUEUE_HOST);
		properties.setProperty(KEY_QUEUE_PORT, STR_QUEUE_PORT);
		
		
		Config config = new Config(properties);
		assertEquals(config.getClientId(), "abc123");
		
		assertNotEquals(config.getClass(), "rubbish");
	}
}
